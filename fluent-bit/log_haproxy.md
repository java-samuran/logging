# Configurar logs Haproxy Centos

En los logs de Centos del haproxy no aparece el nombre del propio host, si no que aparece *localhost*. Para que aparezca el nombre hay que hacer la siguiente configuración:

## Cambio en haproxy.cfg

En el fichero *haproxy.cfg* hay que agregar la siguiente línea en el apartado *global* comentado cualquier otra línea que pueda haber en esta sección (**IMPORTANTE CHROOT: En esa ruta es donde crearemos la carpeta donde se guarde el socket que recoge los logs.**):

```bash
log         /dev/log local0
chroot      /var/lib/haproxy
```

En *default* ponemos que utilice la configuración anterior:

```bash
log     global
```

Creamos la carpeta pertinente:
`sudo mkdir /var/lib/haproxy/dev`

Editamos el fichero de configuración que se encuentra dentro de */etc/rsyslog.d* que haga referencia a haproxy.conf o creamos uno nuevo con el siguiente contenido:

```bash
$AddUnixListenSocket /var/lib/haproxy/dev/log

# Send HAProxy messages to a dedicated logfile
:programname, startswith, "haproxy" {
  /var/log/haproxy.log
  stop
}
```

En caso de tener SELinux activado, hay que crear el modulo e instalarlo.

`vi rsyslog-haproxy.te`

```bash
module rsyslog-haproxy 1.0;

require {
    type syslogd_t;
    type haproxy_var_lib_t;
    class dir { add_name remove_name search write };
    class sock_file { create setattr unlink };
}

#============= syslogd_t ==============
allow syslogd_t haproxy_var_lib_t:dir { add_name remove_name search write };
allow syslogd_t haproxy_var_lib_t:sock_file { create setattr unlink };
```

`sudo yum install checkpolicy`

`checkmodule -M -m rsyslog-haproxy.te -o rsyslog-haproxy.mod`

Con este comando generamos el modulo:
`semodule_package -o rsyslog-haproxy.pp -m rsyslog-haproxy.mod`

Instalamos el modulo que hemos generado:
`sudo semodule -i rsyslog-haproxy.pp`

Reiniciamos los servicios de rsyslog y haproxy:
`systemctl restart rsyslog; systemctl restart haproxy.service`
