package es.samuel.logging;

import org.apache.logging.log4j.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoggingController {

    Logger logger = LogManager.getLogger(LoggingController.class);

    @RequestMapping("/")
    public String index() {
        logger.debug("debug message");
        logger.info("info message");
        logger.warn("warn message");
        logger.error("error message");
        try {
            int i = 1/0;
        }catch(Exception exc) {
            logger.error("error message with stack trace",
                    new Exception("I forced this exception",exc));
        }
        logger.fatal("fatal message");

        return "Howdy! Check out the Logs to see the output...";
    }
}
